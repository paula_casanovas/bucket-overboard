---
title: "Supplemental material: Occupancy modelling." 
subtitle: "Bucket overboard: optimizing marine eDNA sampling at sea from acquisition of biodiversity information towards targeted species detection."
author: "Authors: Ulla von Ammon$^{1}$, Xavier Pochon$^{1,2}$, Paula Casanovas$^{1}$, Branwen Trochel$^{1}$, Martin Zirngibl$^{1}$, Austen Thomas$^{4}$, Anastasija Zaiko$^{1,2}$"
output: 
  word_document:
    reference_docx: SEA2020_Study1.docx
    toc: false
    toc_depth: 5
editor_options: 
  chunk_output_type: console
output_dir: "."
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

<br>

```{r message=FALSE, warning=FALSE, ft.align="left"}
rm(list=ls())
setwd("~/cawthron_projects/marine-biosecurity-toolbox/bucket-overboard")

#Libraries
library(data.table)
library(readxl)
library(tidyverse)
library(readr)
library(unmarked)
library(ggpubr)
library(flextable)

initial_seed <- as.integer(Sys.time())%% 100000
set.seed(54407)
```

<br>

## Get data

The data consist of two datasets, one for each gene: 18S rRNA and COI. Each dataset has three tables, one "metadata" table where the information about the samples is stored, one table with the "taxonomy" information for each ASV identifier, and an "otu" table with the ASV reads per sample. Taxonomy has been checked against the "ncbi" database using the R package and function therein "myTAI::taxonomy". The data for the occupancy modeling were not rarefied. 

<br>

```{r ft.align="left"}
#metadata: site and sample information
metadata18S <- read.csv("data/18S_metazoa_metadata.csv", header = TRUE)
metadataCOI <- read.csv("data/COI_metazoa_metadata.csv", header = TRUE)

#taxonomy
taxonomy18S <- read.csv("data/18S_metazoa_taxonomy.csv")
taxonomyCOI <- read.csv("data/COI_metazoa_taxonomy.csv")

taxonomyCOI[taxonomyCOI$Species %in% "Actiniaria_sp.",]

#otu
otu18S <- read.csv("data/18S_metazoa_otu.csv", header = TRUE)
otuCOI <- read.csv("data/COI_metazoa_otu.csv", header = TRUE)
```

<br>

```{r ft.align="left", echo=FALSE}
flextable(metadata18S[1:3,]) %>% set_caption(caption = "First three rows of the 18S rRNA metadata table.")  %>% fontsize(part = "all", size = 9) %>% autofit() 
```

<br>

```{r ft.align="left", echo=FALSE}
flextable(metadataCOI[1:3,]) %>% set_caption(caption = "First three rows of the COI metadata table.") %>%
fontsize(part = "all", size = 9) %>% autofit() 
```

<br>

```{r ft.align="left", echo=FALSE}
flextable(taxonomy18S[1:3,1:6]) %>% set_caption(caption = "First three rows of the 18S rRNA taxonomy table.") %>% fontsize(part = "all", size = 9) %>% autofit()
```

<br>

```{r ft.align="left", echo=FALSE}
flextable(taxonomyCOI[1:3,1:6]) %>% set_caption(caption = "First three rows of the COI taxonomy table.") %>%
fontsize(part = "all", size = 9) %>% autofit()
```

<br>

```{r ft.align="left", echo=FALSE}
flextable(otu18S[1:3,1:6]) %>% set_caption(caption = "First six three and six columns of the 18S rRNA otu table.") %>% fontsize(part = "all", size = 9) %>% autofit()
```

<br>

```{r ft.align="left", echo=FALSE}
flextable(otuCOI[1:3,1:6]) %>% set_caption(caption = "First six three and six columns of the COI otu table.") %>% fontsize(part = "all", size = 9) %>% autofit()
```

<br>

## Occupancy models for the 18S rRNA dataset

```{r ft.align="left", message=FALSE, warning=FALSE}
#get unique species present in the taxonomy file
uniqueSpecies <- unique(taxonomy18S[!is.na(taxonomy18S$Species),]$Species)

occModelFit18S <- list()
occModelResults18S <- list()

for(i in 1:length(uniqueSpecies))
{
  #select one species
  taxonomySpecies <- taxonomy18S[taxonomy18S$Species %in% uniqueSpecies[i],]
  
  #select from the otu file, the ASVs that match the species above
  otuSpecies <- otu18S[otu18S$X %in% taxonomySpecies$X,]
  otuSpecies <- reshape::melt(otuSpecies, id.vars = "X")

  #sumarise the data by SampleID
  #we could have serveral ASVs per SampleID, and because we are interested in the species level, we add all the values per SampleID
  
  siteSpecies <- otuSpecies %>% 
    group_by(SampleID = variable) %>%
    summarise(eDNA = sum(value, na.rm = TRUE)) %>%
    data.frame()

  #merge the sumarised table above with the metadata file, so we can have the information about Location and Type by SampleID
  SpeciesDetectionData <- merge(siteSpecies,metadata18S[,c("SampleID", "Location", "Type")]) 

  #calculate the overall observed occupancy "rawOccupancy" = % of samples in which a species is present
  rawOccupancy <- nrow(SpeciesDetectionData[SpeciesDetectionData$eDNA>0,]) * 100 /nrow(SpeciesDetectionData)

  #calculate the observed occupancy by Type
  #we first add the total number of samples for each Type
  rawOccupancyType1 <- 
    data.frame(SpeciesDetectionData %>% 
                 group_by(Type) %>% 
                 summarise(n_total = length(eDNA)))
  
  #then we calculate the number of samples in which the species was present
  rawOccupancyType2 <- 
    data.frame(SpeciesDetectionData[SpeciesDetectionData$eDNA>0,] %>% 
                 group_by(Type) %>% 
                 summarise(n_occupy = length(eDNA)))
  
  #we merge both of the above tables
  rawOccupancyType <- merge(rawOccupancyType1,rawOccupancyType2, all.x = TRUE)
  
  #the Types for which the species was absent have NA, we replace that with 0
  rawOccupancyType$n_occupy <- ifelse(is.na(rawOccupancyType$n_occupy),0,rawOccupancyType$n_occupy) 
  
  #now we calculate % observed occupancy by type
  rawOccupancyType$rawOccupancy <- rawOccupancyType$n_occupy * 100/rawOccupancyType$n_total
  
  #transform detection values to binary data -> presence = 1; absence = 0
  SpeciesDetectionData$eDNA  <- ifelse(SpeciesDetectionData$eDNA>0,1,0)

  #single-season site-occupancy analysis
  #we have specified that the detection process is modeled with "Type" as covariate. No covariates are specified for occupancy here.
  SpeciesDetectionData$SampleID <- as.character(SpeciesDetectionData$SampleID)
  
  #with the "ave" function we give a unique number to each sample at a given location
  SpeciesDetectionData$SampleNumber <- ave(SpeciesDetectionData$SampleID, SpeciesDetectionData$Location , FUN = seq_along)
  
  #prepare the species occupancy matrix
  y_species <- dcast(SpeciesDetectionData, Location ~ as.numeric(SampleNumber),value.var = "eDNA")
  y_species <- y_species[,2:ncol(y_species)]
  
  #prepare the observation level covariate matrix (in this case "Type")
  obsCovs_species <- dcast(SpeciesDetectionData, Location ~ as.numeric(SampleNumber),value.var = "Type")
  obsCovs_species <- obsCovs_species[,2:ncol(obsCovs_species)]
  obsCovs_species <- list(Type=obsCovs_species)
  
  #construct the unmarked frame ready for modelling
  wt_species <- unmarkedFrameOccu(y = y_species, obsCovs = obsCovs_species)

  ################################################################
  #fit a single season occupancy model from MacKenzie et al (2002).
  
  #from the unmarked documentation:
  #occu fits the standard occupancy model based on zero-inflated binomial models (MacKenzie et al. 2006, Royle and Dorazio 2008). The occupancy state process (z_i) of site i is modeled as
  
  #z_i ~ Bernoulli(psi_i)
  
  #The observation process is modeled as
  
  #y_ij | z_i ~ Bernoulli(z_i * p_ij)
  
  #By default, covariates of psi_i and p_ij are modeled using the logit link according to the formula argument. 
  fm2 <- occu(~ Type ~ 1, wt_species)
  
  # Because the detection component was modeled with covariates, p is a function, not just a scalar quantity, and so we need to be provide values of our covariates to obtain an estimate of p. 
  
  newData <- data.frame(Type = as.factor(c("B_SR","B_1.2","B_5","net_1.2","net_5")))
  
  # Here, we request the probability of detection given a site is occupied and all possible values of the covariate "Type".
  
  type_detection <- predict(fm2, type = 'det', newdata = newData, appendData=TRUE)
  type_detection$species <-  uniqueSpecies[i]
  type_detection$rawOccupancy <- rawOccupancy
  type_detection$rawOccupancytotal <- rawOccupancyType$rawOccupancy
  
  occModelFit18S[[i]] <- fm2
  occModelResults18S[[i]] <- type_detection
}

#each element of the list above needs to be binded in one table
occModelResultsAll18S <- data.frame(do.call(rbind,occModelResults18S))

occModelResultsAllTaxa18S <- merge(occModelResultsAll18S, unique(taxonomy18S[,c("class", "order", "family", "genus", "Species")]), all.x = TRUE, by.x = "species", by.y = "Species")

occModelResultsAllTaxa18S$BorN <- ifelse(grepl("net", occModelResultsAllTaxa18S$Type), "net","bucket")
occModelResultsAllTaxa18S$Type <- factor(occModelResultsAllTaxa18S$Type, levels = c("B_SR","B_5","B_1.2","net_5","net_1.2"))
```

<br>

## Occupancy models for the COI dataset

```{r ft.align="left", message=FALSE, warning=FALSE}
#get unique species present in the taxonomy file
uniqueSpecies <- unique(taxonomyCOI[!is.na(taxonomyCOI$Species),]$Species)

occModelFitCOI <- list()
occModelResultsCOI <- list()

for(i in 1:length(uniqueSpecies))
{
  #select one species
  taxonomySpecies <- taxonomyCOI[taxonomyCOI$Species %in% uniqueSpecies[i],]
  
  #select from the otu file, the ASVs that match the species above
  otuSpecies <- otuCOI[otuCOI$X %in% taxonomySpecies$X,]
  otuSpecies <- reshape::melt(otuSpecies, id.vars = "X")

  siteSpecies <- otuSpecies %>% 
    group_by(SampleID = variable) %>%
    summarise(eDNA = sum(value, na.rm = TRUE)) %>%
    data.frame()

  SpeciesDetectionData <- merge(siteSpecies,metadataCOI[,c("SampleID", "Location", "Type")]) 

  #calculate the "rawOccupancy" = % of samples in which a species is present
  rawOccupancy <- nrow(SpeciesDetectionData[SpeciesDetectionData$eDNA>0,]) * 100 /nrow(SpeciesDetectionData)
  rawOccupancyType1 <- 
    data.frame(SpeciesDetectionData %>% 
                 group_by(Type) %>% 
                 summarise(n_total = length(eDNA)))
  
  rawOccupancyType2 <- 
    data.frame(SpeciesDetectionData[SpeciesDetectionData$eDNA>0,] %>% 
                 group_by(Type) %>% 
                 summarise(n_occupy = length(eDNA)))
  
  rawOccupancyType <- merge(rawOccupancyType1,rawOccupancyType2, all.x = TRUE)
  rawOccupancyType$n_occupy <- ifelse(is.na(rawOccupancyType$n_occupy),0,rawOccupancyType$n_occupy) 
  rawOccupancyType$rawOccupancy <- rawOccupancyType$n_occupy * 100/rawOccupancyType$n_total
  
  #transform detection values to binary data -> presence = 1; absence = 0
  SpeciesDetectionData$eDNA  <- ifelse(SpeciesDetectionData$eDNA>0,1,0)

  #single-season site-occupancy analysis
  #we have specified that the detection process is modeled with "Type" as covariate. No covariates are specified for occupancy here.
  SpeciesDetectionData$SampleID <- as.character(SpeciesDetectionData$SampleID)
  
  #with the "ave" function we give a unique number to each sample at a given location
  SpeciesDetectionData$SampleNumber <- ave(SpeciesDetectionData$SampleID, SpeciesDetectionData$Location , FUN = seq_along)

  y_species <- dcast(SpeciesDetectionData, Location ~ as.numeric(SampleNumber),value.var = "eDNA")
  y_species <- y_species[,2:ncol(y_species)]
  
  obsCovs_species <- dcast(SpeciesDetectionData, Location ~ as.numeric(SampleNumber),value.var = "Type")
  obsCovs_species <- obsCovs_species[,2:ncol(obsCovs_species)]
  obsCovs_species <- list(Type=obsCovs_species)
  
  wt_species <- unmarkedFrameOccu(y = y_species, obsCovs = obsCovs_species)
  
  fm2 <- occu(~ Type ~ 1, wt_species)
  
  # Because the detection component was modeled with covariates, p is a function, not just a scalar quantity, and so we need to be provide values of our covariates to obtain an estimate of p. 
  
  newData <- data.frame(Type = as.factor(c("B_SR","B_1.2","B_5","net_1.2","net_5")))
  
  # Here, we request the probability of detection given a site is occupied and all possible values of the covariate "Type".
  
  type_detection <- predict(fm2, type = 'det', newdata = newData, appendData=TRUE)
  type_detection$species <-  uniqueSpecies[i]
  type_detection$rawOccupancy <- rawOccupancy
  type_detection$rawOccupancytotal <- rawOccupancyType$rawOccupancy
  
  occModelFitCOI[[i]] <- fm2
  occModelResultsCOI[[i]] <- type_detection
}

occModelResultsAllCOI <- data.frame(do.call(rbind,occModelResultsCOI))

occModelResultsAllTaxaCOI <- merge(occModelResultsAllCOI, unique(taxonomyCOI[,c("class","order", "family", "genus", "Species")]), all.x = TRUE, by.x = "species", by.y = "Species")

occModelResultsAllTaxaCOI$BorN <- ifelse(grepl("net", occModelResultsAllTaxaCOI$Type), "net","bucket")
occModelResultsAllTaxaCOI$Type <- factor(occModelResultsAllTaxaCOI$Type, levels = c("B_SR","B_5","B_1.2","net_5","net_1.2"))
```

<br>

## Occupancy models results

```{r ft.align="left", message=FALSE, warning=FALSE}
#bind tables from the two different barcodes
occModelResultsAllTaxaCOI$barcode <- "COI"
occModelResultsAllTaxa18S$barcode <- "18S rRNA"

occModelResultsAllTaxa <- rbind(occModelResultsAllTaxaCOI,occModelResultsAllTaxa18S)

#we divided the species between those that had values above average rawOccupancy (naive occupancy) and below. 
meanOcc18S <- mean(occModelResultsAllTaxa[occModelResultsAllTaxa$barcode == "18S rRNA",]$rawOccupancytotal)
meanOccCOI <- mean(occModelResultsAllTaxa[occModelResultsAllTaxa$barcode == "COI",]$rawOccupancytotal)
 
occModelResultsAllTaxa$above_average <- NA

occModelResultsAllTaxa[occModelResultsAllTaxa$barcode %in% c("18S rRNA"),]$above_average <- ifelse(occModelResultsAllTaxa[occModelResultsAllTaxa$barcode %in% c("18S rRNA"),]$rawOccupancytotal>=meanOcc18S, "high-occupancy","low-occupancy")

occModelResultsAllTaxa[occModelResultsAllTaxa$barcode %in% c("COI"),]$above_average <- ifelse(occModelResultsAllTaxa[occModelResultsAllTaxa$barcode %in% c("COI"),]$rawOccupancytotal>=meanOccCOI, "high-occupancy","low-occupancy")
``` 

### Comparison among Types

```{r message=FALSE, warning=FALSE, ft.align="left"}
#18S rRNA comparisons for species with hight occupancy
list_comparisons_18S_above <- compare_means(Predicted ~ Type, data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "high-occupancy" &
                       occModelResultsAllTaxa$barcode == "18S rRNA",])
list_comparisons_18S_above_table <- list_comparisons_18S_above

#chose only significant differences for showing in plots
list_comparisons_18S_above <- list_comparisons_18S_above[list_comparisons_18S_above$p.adj<=0.05,]
list_comparisons_18S_above <- data.frame(list_comparisons_18S_above$group1, list_comparisons_18S_above$group2)
comparisons_18S_above <- as.list(as.data.frame(t(list_comparisons_18S_above)))

#COI comparisons for species with hight occupancy
list_comparisons_COI_above <- compare_means(Predicted ~ Type, data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "high-occupancy" &
                       occModelResultsAllTaxa$barcode == "COI",])
list_comparisons_COI_above_table <- list_comparisons_COI_above
  
list_comparisons_COI_above <- list_comparisons_COI_above[list_comparisons_COI_above$p.adj<=0.05,]
list_comparisons_COI_above <- data.frame(list_comparisons_COI_above$group1, list_comparisons_COI_above$group2)
comparisons_COI_above <- as.list(as.data.frame(t(list_comparisons_COI_above)))

#18S rRNA comparisons for species with low occupancy
list_comparisons_18S_below <- compare_means(Predicted ~ Type, data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "low-occupancy" & occModelResultsAllTaxa$barcode == "18S rRNA",])
list_comparisons_18S_below_table <- list_comparisons_18S_below

list_comparisons_18S_below <- list_comparisons_18S_below[list_comparisons_18S_below$p.adj<=0.05,]
list_comparisons_18S_below <- data.frame(list_comparisons_18S_below$group1, list_comparisons_18S_below$group2)
comparisons_18S_below <- as.list(as.data.frame(t(list_comparisons_18S_below)))

#COI comparisons for species with low occupancy
list_comparisons_COI_below <- compare_means(Predicted ~ Type, data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "low-occupancy" & occModelResultsAllTaxa$barcode == "COI",],)
list_comparisons_COI_below_table <- list_comparisons_COI_below

list_comparisons_COI_below <- list_comparisons_COI_below[list_comparisons_COI_below$p.adj<=0.05,]
list_comparisons_COI_below <- data.frame(list_comparisons_COI_below$group1, list_comparisons_COI_below$group2)
comparisons_COI_below <- as.list(as.data.frame(t(list_comparisons_COI_below)))
```

<br>

```{r message=FALSE, warning=FALSE, ft.align="left", echo=FALSE}
#18S rRNA comparisons for species with hight occupancy
list_comparisons_18S_above_table$p <- round(list_comparisons_18S_above_table$p,4)
list_comparisons_18S_above_table$p.adj <- round(list_comparisons_18S_above_table$p.adj,4)

flextable(list_comparisons_18S_above_table[,c(2:5,7)]) %>% set_caption(caption = "18S rRNA comparisons for species with hight occupancy (Wilcoxon Rank Sum Test).")  %>% fontsize(part = "all", size = 9) %>% autofit() 
```

<br>

```{r message=FALSE, warning=FALSE, ft.align="left", echo=FALSE}
#COI comparisons for species with hight occupancy
list_comparisons_COI_above_table$p <- round(list_comparisons_COI_above_table$p,4)
list_comparisons_COI_above_table$p.adj <- round(list_comparisons_COI_above_table$p.adj,4)

flextable(list_comparisons_COI_above_table[,c(2:5,7)]) %>% set_caption(caption = "COI comparisons for species with hight occupancy (Wilcoxon Rank Sum Test).")  %>% fontsize(part = "all", size = 9) %>% autofit() 
```

<br>

```{r message=FALSE, warning=FALSE, ft.align="left", echo=FALSE}
#18S rRNA comparisons for species with low occupancy
list_comparisons_18S_below_table$p <- round(list_comparisons_18S_below_table$p,4)
list_comparisons_18S_below_table$p.adj <- round(list_comparisons_18S_below_table$p.adj,4)

flextable(list_comparisons_18S_below_table[,c(2:5,7)]) %>% set_caption(caption = "18S rRNA comparisons for species with low occupancy (Wilcoxon Rank Sum Test).")  %>% fontsize(part = "all", size = 9) %>% autofit() 
```

<br>

```{r message=FALSE, warning=FALSE, ft.align="left", echo=FALSE}
#COI comparisons for species with low occupancy
list_comparisons_COI_below_table$p <- round(list_comparisons_COI_below_table$p,4)
list_comparisons_COI_below_table$p.adj <- round(list_comparisons_COI_below_table$p.adj,4)

flextable(list_comparisons_COI_below_table[,c(2:5,7)]) %>% set_caption(caption = "COI comparisons for species with low occupancy (Wilcoxon Rank Sum Test).")  %>% fontsize(part = "all", size = 9) %>% autofit() 
```

<br>

```{r ft.align="left", echo=FALSE, fig.width=16, fig.height=12, message=FALSE, warning=FALSE}
#plot of detection probabilities by Type
give.n.above <- function(x){return(c(y =1.1, label = length(x)))}
give.n.below <- function(x){return(c(y =-0.1, label = length(x)))}

ggplot(aes(y = Predicted, x = Type), data = occModelResultsAllTaxa) +
  geom_boxplot(size=0.8, alpha=0.3, col="black") +
  ylim(-0.1,2.1) +
  labs(x="Type", y="Probability of detection")  +
  theme_bw(base_size = 28) +
  
  stat_compare_means(data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "high-occupancy" & occModelResultsAllTaxa$barcode == "COI",], comparisons = comparisons_COI_above,label = "p.signif") +
  stat_summary(aes(y = Predicted, x = Type), data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "high-occupancy" & occModelResultsAllTaxa$barcode == "COI",], fun.data = give.n.below, geom = "text", fun = median, position = position_dodge(width = 0.75),cex=8) +
  
  stat_compare_means(data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "high-occupancy" & occModelResultsAllTaxa$barcode == "18S rRNA",], comparisons = comparisons_18S_above,label = "p.signif") +
  stat_summary(aes(y = Predicted, x = Type), data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "high-occupancy" & occModelResultsAllTaxa$barcode == "18S rRNA",], fun.data = give.n.below, geom = "text", fun = median, position = position_dodge(width = 0.75),cex=8) +
  
  stat_compare_means(data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "low-occupancy" & occModelResultsAllTaxa$barcode == "COI",], comparisons = comparisons_COI_below,label = "p.signif") +
  stat_summary(aes(y = Predicted, x = Type), data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "low-occupancy" & occModelResultsAllTaxa$barcode == "COI",], fun.data = give.n.below, geom = "text", fun = median, position = position_dodge(width = 0.75),cex=8) +
  
  stat_compare_means(data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "low-occupancy" & occModelResultsAllTaxa$barcode == "18S rRNA",], comparisons = comparisons_18S_below,label = "p.signif") +
  stat_summary(aes(y = Predicted, x = Type), data = occModelResultsAllTaxa[occModelResultsAllTaxa$above_average == "low-occupancy" & occModelResultsAllTaxa$barcode == "18S rRNA",], fun.data = give.n.below, geom = "text", fun = median, position = position_dodge(width = 0.75),cex=8) +
  
  facet_grid(rows = vars(barcode), cols = vars(above_average))
```

<br>

```{r ft.align="left", echo=FALSE, fig.width=16, fig.height=12, message=FALSE, warning=FALSE}
#plot of order/families in each type

#for 18S rRNA
  otu_melt18S <- reshape::melt(otu18S, id.vars = "X")
  otu_taxonomy18S <- merge(otu_melt18S,taxonomy18S)
  otu_taxonomy_type18S <- merge(otu_taxonomy18S,metadata18S, by.x="variable", by.y = "SampleID")

  occModelResultsAllTaxa_18S <- occModelResultsAllTaxa[occModelResultsAllTaxa$barcode %in% "18S rRNA",]
  otu_taxonomy_results18S <- merge(otu_taxonomy_type18S, occModelResultsAllTaxa_18S, by.x = c("Species", "Type"), by.y = c("species", "Type"))

  family_reads18S <- otu_taxonomy_results18S %>%
    group_by(family.x, phylum,above_average, Type) %>%
    summarise(eDNA = sum(value, na.rm = TRUE)) %>%
    data.frame()

total_reads_18S_high <- sum(family_reads18S[family_reads18S$above_average %in% "high-occupancy",]$eDNA)
total_reads_18S_low <- sum(family_reads18S[family_reads18S$above_average %in% "low-occupancy",]$eDNA)

family_reads18S$eDNApercent <- NA
family_reads18S$five_percent <- NA

family_reads18S[family_reads18S$above_average %in% "high-occupancy",]$eDNApercent <- family_reads18S[family_reads18S$above_average %in% "high-occupancy",]$eDNA/total_reads_18S_high*100

family_reads18S[family_reads18S$above_average %in% "high-occupancy",]$five_percent <- ifelse(family_reads18S[family_reads18S$above_average %in% "high-occupancy",]$eDNApercent<2, "Other", family_reads18S[family_reads18S$above_average %in% "high-occupancy",]$family.x)

family_reads18S[family_reads18S$above_average %in% "low-occupancy",]$eDNApercent <- family_reads18S[family_reads18S$above_average %in% "low-occupancy",]$eDNA/total_reads_18S_low*100

family_reads18S[family_reads18S$above_average %in% "low-occupancy",]$five_percent <- ifelse(family_reads18S[family_reads18S$above_average %in% "low-occupancy",]$eDNApercent<2, "Other", family_reads18S[family_reads18S$above_average %in% "low-occupancy",]$family.x)

family_reads18S$phylum_other <- ifelse(family_reads18S$five_percent %in% "Other", "Other", family_reads18S$phylum)

family_reads_18S_summary <- family_reads18S %>% 
    group_by(five_percent, phylum_other, above_average, Type) %>%
    summarise(eDNApercent = sum(eDNApercent, na.rm = TRUE),
              eDNA = sum(eDNA, na.rm = TRUE)) %>%
    data.frame()

#for COI
  otu_meltCOI <- reshape::melt(otuCOI, id.vars = "X")
  otu_taxonomyCOI <- merge(otu_meltCOI,taxonomyCOI)
  otu_taxonomy_typeCOI <- merge(otu_taxonomyCOI,metadataCOI, by.x="variable", by.y = "SampleID")

  occModelResultsAllTaxa_COI <- occModelResultsAllTaxa[occModelResultsAllTaxa$barcode %in% "COI",]
  otu_taxonomy_resultsCOI <- merge(otu_taxonomy_typeCOI, occModelResultsAllTaxa_COI, by.x = c("Species", "Type"), by.y = c("species", "Type"))

  family_readsCOI <- otu_taxonomy_resultsCOI %>%
    group_by(family.x, phylum,above_average, Type) %>%
    summarise(eDNA = sum(value, na.rm = TRUE)) %>%
    data.frame()

total_reads_COI_high <- sum(family_readsCOI[family_readsCOI$above_average %in% "high-occupancy",]$eDNA)
total_reads_COI_low <- sum(family_readsCOI[family_readsCOI$above_average %in% "low-occupancy",]$eDNA)

family_readsCOI$eDNApercent <- NA
family_readsCOI$five_percent <- NA

family_readsCOI[family_readsCOI$above_average %in% "high-occupancy",]$eDNApercent <- family_readsCOI[family_readsCOI$above_average %in% "high-occupancy",]$eDNA/total_reads_COI_high*100

family_readsCOI[family_readsCOI$above_average %in% "high-occupancy",]$five_percent <- ifelse(family_readsCOI[family_readsCOI$above_average %in% "high-occupancy",]$eDNApercent<2, "Other", family_readsCOI[family_readsCOI$above_average %in% "high-occupancy",]$family.x)

family_readsCOI[family_readsCOI$above_average %in% "low-occupancy",]$eDNApercent <- family_readsCOI[family_readsCOI$above_average %in% "low-occupancy",]$eDNA/total_reads_COI_low*100

family_readsCOI[family_readsCOI$above_average %in% "low-occupancy",]$five_percent <- ifelse(family_readsCOI[family_readsCOI$above_average %in% "low-occupancy",]$eDNApercent<2, "Other", family_readsCOI[family_readsCOI$above_average %in% "low-occupancy",]$family.x)

family_readsCOI$phylum_other <- ifelse(family_readsCOI$five_percent %in% "Other", "Other", family_readsCOI$phylum)

family_reads_COI_summary <- family_readsCOI %>% 
    group_by(five_percent, phylum_other, above_average, Type) %>%
    summarise(eDNApercent = sum(eDNApercent, na.rm = TRUE),
              eDNA = sum(eDNA, na.rm = TRUE)) %>%
    data.frame()

family_reads_18S_summary$gene <- "18S rRNA"
family_reads_COI_summary$gene <- "COI"

family_reads_twogenes <- rbind(family_reads_COI_summary,family_reads_18S_summary)

library(RColorBrewer)
mypal <- colorRampPalette(brewer.pal(10, "Greens"))
mypal2 <- colorRampPalette(brewer.pal(10, "Reds"))
mypal3 <- colorRampPalette(brewer.pal(10, "Purples"))
mypal4 <- colorRampPalette(brewer.pal(10, "Oranges"))
mypal5 <- colorRampPalette(brewer.pal(10, "YlOrBr"))
mypal6 <- colorRampPalette(brewer.pal(10, "Greys"))

# family_reads_twogenes$phylum_other <- ifelse(family_reads_twogenes$five_percent %in% "Other", "Other", family_reads_twogenes$phylum)
# uniquephylum<- unique(data.frame(family_reads_twogenes$phylum_other, family_reads_twogenes$five_percent))
# uniquephylum[order(uniquephylum$family_reads_twogenes.phylum_other),]$family_reads_twogenes.five_percent

family_reads_twogenes$phylum_family <- paste(family_reads_twogenes$phylum_other,":", family_reads_twogenes$five_percent)
family_reads_twogenes$phylum_family <- factor(family_reads_twogenes$phylum_family, levels = c(sort(unique(family_reads_twogenes$phylum_family))[-26], "Other : Other"))


ggplot(family_reads_twogenes,
  aes(fill=phylum_family, y=eDNA, x=Type, phylum_other)) +
  geom_bar(position="stack", stat="identity") +
  labs(x="Type", y="Number of reads per Family", fill = "Families")  +
  scale_fill_manual(values = c(mypal(8)[2:4], mypal2(9), mypal2(9)[1],mypal3(8)[2:4], mypal4(8)[2:5], mypal4(8)[7], mypal5(8)[2], mypal(9)[7:9],mypal3(9)[6:9], mypal6(8)[3], mypal6(8)[8])) +
  theme_bw(base_size = 18) +
  facet_grid(cols = vars(gene), rows = vars(above_average), scales = "free")

# same plot per number of species
# create a dataset
countFamily <- occModelResultsAllTaxa %>% 
    group_by(Type, barcode, above_average,family) %>%
    summarise(family_n = length(family)) %>%
    data.frame()

countOrder <- occModelResultsAllTaxa %>% 
    group_by(Type, barcode, above_average,order) %>%
    summarise(family_n = length(order)) %>%
    data.frame()

countOrder <- occModelResultsAllTaxa %>% 
    group_by(Type, barcode, above_average,class) %>%
    summarise(family_n = length(family)) %>%
    data.frame()

countClass <- occModelResultsAllTaxa %>% 
    group_by(Type, barcode, above_average,class) %>%
    summarise(family_n = length(family)) %>%
    data.frame()

ggplot(aes(fill=class, y=family_n, x=Type), data = countClass) +
  geom_bar(position="stack", stat="identity") +
  labs(x="Type", y="Number of species per Class")  +
  theme_bw(base_size = 18) +
   facet_grid(rows = vars(barcode), cols = vars(above_average))
```

<br>

```{r ft.align="left", echo=FALSE, fig.width=16, fig.height=12, message=FALSE, warning=FALSE}
#plot of observed occupancy vs. detection probabilities

ggplot(aes(y = Predicted, x = rawOccupancytotal), data = occModelResultsAllTaxa) +
  geom_point(size=1.2, alpha=0.8) +
  labs(x="Observed occupancy (%)", y="Probability of detection")  +
  theme_bw(base_size = 12) +
  ylim(0,1) +
  theme(legend.key.size = unit(1, "cm"), axis.ticks = element_blank(), legend.position = "none") + 
  facet_grid(cols = vars(Type), rows = vars(barcode)) 
```

<br>

```{r ft.align="left", echo=FALSE, fig.width=16, fig.height=16, message=FALSE, warning=FALSE}
#plot of detection probabilities by Type, for example classes

numberClassOverall <- occModelResultsAllTaxa %>% 
  group_by(class) %>%
  summarise(class_n = length(unique(species))) %>%
  data.frame()

flextable(numberClassOverall[order(-numberClassOverall$class_n,numberClassOverall$class),][1:6,]) %>% set_caption(caption = "Number of species in the six more numerous classes")  %>% fontsize(part = "all", size = 9) %>% autofit() 

occClassLimited <- occModelResultsAllTaxa[occModelResultsAllTaxa$class %in% c("Bivalvia", "Hexanauplia","Polychaeta"),]

ggplot() +
  geom_boxplot(aes(y = Predicted, x = Type), data = occClassLimited[occClassLimited$above_average == "low-occupancy",], size=0.8, alpha=0.4, col="grey") +
  geom_boxplot(aes(y = Predicted, x = Type), data = occClassLimited[occClassLimited$above_average == "high-occupancy",], size=0.8, alpha=0.4) +
  labs(x="Type", y="Probability of detection")  +
  stat_summary(aes(y = Predicted, x = Type), data = occClassLimited[occClassLimited$above_average == "high-occupancy",], fun.data = give.n.above, geom = "text", fun = median, position = position_dodge(width = 0.75),cex=8) +
  stat_summary(aes(y = Predicted, x = Type), data = occClassLimited[occClassLimited$above_average == "low-occupancy",], fun.data = give.n.below, geom = "text", fun = median, position = position_dodge(width = 0.75), col="grey",cex=8) +
  theme_bw(base_size = 25) +
  facet_grid(cols = vars(class), rows = vars(barcode)) 
```

<br>

```{r ft.align="left", echo=FALSE, fig.width=16, fig.height=16, message=FALSE, warning=FALSE}
#plot of detection probabilities by Type, for example orders of Polychaeta.

oocPplychaeta <- occModelResultsAllTaxa[occModelResultsAllTaxa$class %in% c("Polychaeta"),]
#unique(oocPplychaeta$Genus)
#unique(oocPplychaeta$order)

oocPplychaeta[!is.na(oocPplychaeta$order),]$order <- paste("Order: ", oocPplychaeta[!is.na(oocPplychaeta$order),]$order, sep="")
oocPplychaeta[is.na(oocPplychaeta$order),]$order <- paste("Family: ", oocPplychaeta[is.na(oocPplychaeta$order),]$family, sep="")

cc <- scales::seq_gradient_pal("#00549f", "#4aa890","Lab")(seq(0,1,length.out=20))

library(RColorBrewer)
getPalette = colorRampPalette(brewer.pal(9, "Set1"))
colourCount = length(unique(oocPplychaeta[oocPplychaeta$order %in% c("Order: Spionida", "Order: Spionida", "Order: Sabellida"),]$species))+5

ggplot(aes(y = Predicted, x = Type), data = oocPplychaeta[oocPplychaeta$order %in% c("Order: Spionida", "Order: Spionida", "Order: Sabellida"),]) +
  geom_boxplot(aes(y = Predicted, x = Type), data = oocPplychaeta[oocPplychaeta$order %in% c("Order: Spionida", "Order: Spionida", "Order: Sabellida") & oocPplychaeta$barcode %in% c("18S rRNA"),], size=1.2, alpha=0.4, col="grey") +
  geom_point(aes(y = Predicted, x = Type, color = species), size=4, alpha=0.8) +
  labs(x="Type", y="Probability of detection")  + 
  theme_bw(base_size = 18) +
  scale_colour_manual(values=getPalette(colourCount)) +
  theme(legend.position="none") +
  facet_grid(cols = vars(order), rows = vars(barcode)) 
```

<br>

## Detection probability for Sabella spallanzanii (ddPCR, 18S rRNA, COI)

```{r ft.align="left", echo=FALSE, fig.width=16, fig.height=16, message=FALSE, warning=FALSE}
# taxonomySpecies <- taxonomy18S[taxonomy18S$Species %in% "Sabella_spallanzanii",]
# otuSpecies <- otu18S[otu18S$X %in% taxonomySpecies$X,]
# otuSpecies <- reshape::melt(otuSpecies, id.vars = "X")
# siteSpecies <- otuSpecies %>% 
#     group_by(SampleID = variable) %>%
#     summarise(eDNA = sum(value, na.rm = TRUE)) %>%
#     data.frame()
# SpeciesDetectionData <- merge(siteSpecies,metadata18S[,c("SampleID", "Location", "Type")]) 
# SpeciesDetectionData[SpeciesDetectionData$eDNA>0,]

#Occupancy model for ddPCR of Sabella spallanzanii
SpeciesDetectionDataSspallanzanii <- read.csv("data/Sabella_spallanzanii_ddPCR.csv")

#transform detection values to binary data -> presence = 1; absence = 0
SpeciesDetectionDataSspallanzanii$ddPCR  <- ifelse(SpeciesDetectionDataSspallanzanii$ddPCR>0,1,0)

#single-season site-occupancy analysis
#we have specified that the detection process is modeled with "Type" as covariate. No covariates are specified for occupancy here.
  SpeciesDetectionDataSspallanzanii$SampleID <- as.character(SpeciesDetectionDataSspallanzanii$SampleID)
  
  #with the "ave" function we give a unique number to each sample at a given location
  SpeciesDetectionDataSspallanzanii$SampleNumber <- ave(SpeciesDetectionDataSspallanzanii$SampleID, SpeciesDetectionDataSspallanzanii$Location , FUN = seq_along)

  y_species <- dcast(SpeciesDetectionDataSspallanzanii, Location ~ as.numeric(SampleNumber),value.var = "ddPCR")
  y_species <- y_species[,2:ncol(y_species)]
  
  obsCovs_species <- dcast(SpeciesDetectionDataSspallanzanii, Location ~ as.numeric(SampleNumber),value.var = "Type")
  obsCovs_species <- obsCovs_species[,2:ncol(obsCovs_species)]
  obsCovs_species <- list(Type=obsCovs_species)
  
  wt_species <- unmarkedFrameOccu(y = y_species, obsCovs = obsCovs_species)
  
  fm2 <- occu(~ Type ~ 1, wt_species)
  
  # Because the detection component was modeled with covariates, p is a function, not just a scalar quantity, and so we need to be provide values of our covariates to obtain an estimate of p. 
  
  newData <- data.frame(Type = as.factor(c("B_SR","B_1.2","B_5","net_1.2","net_5")))
  
  # Here, we request the probability of detection given a site is occupied and all possible values of the covariate "Type".
  
  type_detection <- predict(fm2, type = 'det', newdata = newData, appendData=TRUE)
  type_detection$species <-  "Sabella_spallanzanii"

type_detection$detection_probability_ddPCR <- round(type_detection$Predicted,5)
type_detection$SE_ddPCR  <- round(type_detection$SE,5)
type_detection$lower_ddPCR  <- round(type_detection$lower,5)
type_detection$upper_ddPCR  <- round(type_detection$upper,5)

type_detection <- type_detection[,c("Type", "detection_probability_ddPCR", "SE_ddPCR" , "lower_ddPCR", "upper_ddPCR")]

#18S rRNA detections of Sabella spallanzanii
Sabella_spallanzanii_detections_18S <- occModelResultsAllTaxa[occModelResultsAllTaxa$species%in%"Sabella_spallanzanii",c(2:6)]

Sabella_spallanzanii_detections_18S$detection_probability_18S <- round(Sabella_spallanzanii_detections_18S$Predicted,5)
Sabella_spallanzanii_detections_18S$SE_18S  <- round(Sabella_spallanzanii_detections_18S$SE,5)
Sabella_spallanzanii_detections_18S$lower_18S  <- round(Sabella_spallanzanii_detections_18S$lower,5)
Sabella_spallanzanii_detections_18S$upper_18S  <- round(Sabella_spallanzanii_detections_18S$upper,5)

Sabella_spallanzanii_detections_18S <- Sabella_spallanzanii_detections_18S[,c("Type", "detection_probability_18S", "SE_18S" , "lower_18S", "upper_18S")]

Sabella_spallanzanii_detections <- merge(Sabella_spallanzanii_detections_18S, type_detection, by = "Type")

#Sabella_pavonina for COI is treated as Sabella_spallanzanii
Sabella_spallanzanii_detections_COI <- occModelResultsAllTaxa[occModelResultsAllTaxa$species%in%"Sabella_pavonina",c(2:6)]

Sabella_spallanzanii_detections_COI$detection_probability_COI <- round(Sabella_spallanzanii_detections_COI$Predicted,5)
Sabella_spallanzanii_detections_COI$SE_COI  <- round(Sabella_spallanzanii_detections_COI$SE,5)
Sabella_spallanzanii_detections_COI$lower_COI  <- round(Sabella_spallanzanii_detections_COI$lower,5)
Sabella_spallanzanii_detections_COI$upper_COI  <- round(Sabella_spallanzanii_detections_COI$upper,5)

Sabella_spallanzanii_detections_COI <- Sabella_spallanzanii_detections_COI[,c("Type", "detection_probability_COI", "SE_COI" , "lower_COI", "upper_COI")]

Sabella_spallanzanii_detections <- merge(Sabella_spallanzanii_detections, Sabella_spallanzanii_detections_COI, by = "Type")

Sabella_spallanzanii_detections$`Detection probability 18S rRNA` <- paste(Sabella_spallanzanii_detections$detection_probability_18S, " (", Sabella_spallanzanii_detections$SE_18S,")", sep="")

Sabella_spallanzanii_detections$`Detection probability COI` <- paste(Sabella_spallanzanii_detections$detection_probability_COI, " (", Sabella_spallanzanii_detections$SE_COI,")", sep="")

Sabella_spallanzanii_detections$`Detection probability ddPCR` <- paste(Sabella_spallanzanii_detections$detection_probability_ddPCR, " (", Sabella_spallanzanii_detections$SE_ddPCR,")", sep="")

flextable(Sabella_spallanzanii_detections[,c(1,14:16)]) %>% set_caption(caption = "Detection probabilities for Sabella spallanzanii.")  %>% fontsize(part = "all", size = 9) %>% autofit() 
```

<br>

## Detection probabilities for all Metazoan species found

```{r ft.align="left", echo=FALSE, fig.width=16, fig.height=16, message=FALSE, warning=FALSE}
#Table of all detection probability results
occModelResultsAllTaxa <- occModelResultsAllTaxa[,c("species", "family", "order", "class", "barcode",  "Type", "rawOccupancytotal", "above_average", "Predicted", "SE"),]
names(occModelResultsAllTaxa) <- c("species", "family", "order", "class", "gene","Type", "observed occupancy", "low or high occupancy","probability of detection","SE")

write.csv(occModelResultsAllTaxa,"occModelResultsAllTaxa.csv", row.names = FALSE)

occModelResultsAllTaxa$`probability of detection` <- round(occModelResultsAllTaxa$`probability of detection`,4)
occModelResultsAllTaxa$SE <- round(occModelResultsAllTaxa$SE,4)
# occModelResultsAllTaxa$lower <- round(occModelResultsAllTaxa$lower,4)
# occModelResultsAllTaxa$upper <- round(occModelResultsAllTaxa$upper,4)

flextable(occModelResultsAllTaxa) %>% set_caption(caption = "Detection probabilities for all Metazoan species found.")  %>% fontsize(part = "all", size = 9) %>% autofit() 
```
